

The end user of the application should be able to upload a CSV file and the output
data will be serialized.
Objective:
1. Upload CSV file (the CSV file can be accessed from here)
2. Serialize the data (refer to the output)
3. The output should be in json format
4. Input format folder has sample input example
5. Output format folder has sample output example
1.create database name fileupload password lightcastle in db folder
2.php artisan migrate  Or You may import fileupload.sql file from root directory
3.php artisan serve 
