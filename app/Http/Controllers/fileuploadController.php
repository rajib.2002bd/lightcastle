<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use App\Models\Data_output;
use App\Models\document;

class fileuploadController extends Controller
{
    public function new_file(Request $request)
	{
		
		// $path = $request->file('csvfilename')->storePublicly('uploadfolder');
		 
		 $count =0;
		 if ($request->hasfile('csvfilename')) {

            	$file = $request->file('csvfilename');
				/*
				$file= $request->file('csvfilename');
                 $original_name=$file->getClientOriginalName();
                $last_file_count =1;
                $extension = $file->guessExtension();
                $name = $original_name;
                $full_path = $name;
                \Log::info($full_path);
				$file->move($full_path);
                //        $data[] = $name;
            */
			
			  $destinationPath = 'upload';
              $file->move($destinationPath,$file->getClientOriginalName());
            
			  $file_title = $file->getClientOriginalName();
			  $full_path = $destinationPath."/".$file_title;

				$file = fopen($full_path,"r");
				\Log::info('file open success');
				Data_output::truncate();
				
				while (($data = fgetcsv ( $file, 1000, ',' )) !== FALSE ) {
                 \Log::info('get csv success');
//				  Sl,Date,Time,Product,Side,Qty,Exe Price,Acct
                  if($count>0){
				  $new_data = new Data_output();
                  $new_data->Sl = $data[0];
			      
				  $temp_date_time = $data[1].' '.$data[2];
				  $date_time_create_form = Carbon::createFromFormat('d/m/y H:i:s', $temp_date_time);
				  //$date_time_create_form =now();
				  $new_data->Dt =  $date_time_create_form->format('Y-m-d');
				  $new_data->Tm = $data[2];
				 $new_data->Transaction_at = $date_time_create_form->format('Y-m-d H:i:s');
				  $new_data->Product =$data[3];
				  $new_data->Side =$data[4];
				  $new_data->Qty =$data[5];
				  $new_data->Price =$data[6];
				  $new_data->Acct =$data[7];
				  $new_data->Timestmp =  $date_time_create_form->timestamp;
				  $new_data->Group_AC = $new_data->Acct."_".$new_data->Product."_".$date_time_create_form->format('Ymd');
				  $new_data->save ();	
                  \Log::info('file csv upload success');				  
				  } else {
					  
					  $count = 1;
				  }
    		  
				}
				fclose ($file);
			
				$output =  Data_output::Select('Sl','Timestmp','Transaction_at','Product','Qty','Price','Side','Acct','Group_AC','Remark1','Remark2',
				'Remark3','Remark4','Remark5')->where('Sl','>', 0)->get()->groupBy('Group_AC')->sortBy('Transaction_at');
				return response()->json($output);
			//	return view('upload', ['title'=>$file_title,'upload_file'=>$full_path ]);
		} else {
			
			return redirect('/');
			
			
		}
		 
		 
		 
		
      
	    
			
			
			
			
			
			
			
			
			    
				
				
			
			
			
			
			
			//return redirect('agents')->with('Success', 'Agent Added');
			
			
		
	}
}
