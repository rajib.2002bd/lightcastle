<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Data_output extends Model
{
    use HasFactory;
	protected $fillable = [
    'Sl','Dt','Tm','Timestmp',
  'Transaction_at','Product','Qty','Price','Side','Acct','Stat','Remark1','Remark2','Remark3','Remark4','Remark5'
		
		
    ];
	
}
