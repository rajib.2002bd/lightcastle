<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataOutputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_outputs', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
			$table->integer('Sl');
			 $table->date('Dt'); 
			$table->time('Tm');
			 $table->string('Timestmp',40)->nullable();
			 $table->dateTime('Transaction_at')->nullable();
			 $table->string('Product',100); 
			 $table->integer('Qty'); 
			 $table->float('Price', 10, 2);
			 $table->string('Side',20); 
			 $table->string('Acct',20); 
			 $table->string('GROUP_AC')->nullable(); 
			 $table->string('Stat')->nullable(); 
			 $table->string('Remark1')->nullable(); 
			 $table->string('Remark2')->nullable(); 
			 $table->string('Remark3')->nullable(); 
			 $table->string('Remark4')->nullable(); 
			 $table->string('Remark5')->nullable();
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_outputs');
    }
}
